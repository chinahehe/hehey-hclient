# hehey-hclient

#### 介绍
hehey-hclient 是一个python 请求客户请求工具组件,常用于接口的调用

#### 基本示例

```python


from hclient.Client import Client;

client = Client({
    'sites':{
        'mba':{
            'host':'http://api.xxxx.cn/',
            'method':'POST',
            'response':{
                'clazz':'service',
                'format':"json"
            }
        },
        # socket 请求
        'cba':{
            'transport':"socket",
            'clazz':'http',
            'host':'http://api.xxxx.cn/',
            'method':'POST',
            'response':{
                'clazz':'service',
                'format':"json"
            }
        }
    }
})


# 实例1:GET HTTP 请求,支持OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
response = client.get('http://www.baidu.com/',{'user_id':2}).send()

#获取返回结果
html = response.getData()
# 获取原始结果
html = response.getContent()


# 实例2:设置request,respone 数据为json格式,json 可为类命名空间

response = client.post('http://api.baidu.com/', {'user_id': 2}).setFormat('json').send();
data = response.setFormat('json').getData()

# 实例3:批量请求请求

request1 = client.service('mba','system/site/getConfInfo',{"id":1})
request2 = client.service('mba','system/site/getConfInfo',{"id":1})
requests = {
    'user1': request1,
    'user2': request2
}

responses = client.batchSend(requests);
result = responses['user1'].setFormat('json').getData();

# 实例4:验证http 错误
response = client.post('http://www.baidu.com/',{'user_id':2}).send();
# 验证是否错误,验证网络,解析数据,Transport（传输层） 是否有错误
response.hasError()

# 验证是否网络错误,主要验证header http-code 状态码 是否等于20x
response.hasNetworkError()

# 获取错误信息
response.getErrors() # 获取全部错误
response.getFirstError() # 获取首个错误信息


# 实例5 api 接口示例
response = client.service('mba','system/site/getConfInfo',{"id":1}).send()
# 基本配置
sites = {
     # 站点id
     'mba':{
            'clazz':'request class name',# request 类名,比如http
            'host':'http://api.xxxx.cn/',# 接口host 地址
            'format':'json',# 数据格式类型,
            'headers':{},# 默认header 信息
            'options':{},# 传输协议配置
            'method':'POST',
            'response':{
                'clazz':'service',
                'format':"json",# 响应内容数据格式
            }
        },
   }


# 示例6 socket 的方式发送api请求

response = client.service('cba','system/site/getConfInfo',{"id":1}).send()
html = response.getContent()

  
```
