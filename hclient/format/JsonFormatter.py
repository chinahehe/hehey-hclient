
from .Formatter import Formatter
import json

class JsonFormatter(Formatter):


    def format(self,request):

        request.setContent(json.dumps(request.getContent()));

        pass
