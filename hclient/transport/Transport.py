
import abc
from ..utils.helper import CommonUtil
"""
 * 传输协议基类
 *<B>说明：</B>
 *<pre>
 *  使用
 *</pre>
 *<B>示例：</B>
 *<pre>
 *  略
 *</pre>
 *<B>日志：</B>
 *<pre>
 *  略
 *</pre>
 *<B>注意事项：</B>
 *<pre>
 *  略
 *</pre>
"""
class Transport:

    @classmethod
    def make(cls, transportName) -> 'DbConnection':
        if not transportName:
            raise RuntimeError('transportName is empty')

        if transportName.find('.') == -1:
            transportClazzName = CommonUtil.ucfirst(transportName) + 'Transport'
            transportClazz = __package__ + "." + transportClazzName + '.' + transportClazzName
        else:
            transportClazz = transportName

        transportMeta = CommonUtil.getClassMeta(transportClazz)

        return transportMeta()


    @abc.abstractmethod
    def send(self,request):
        pass

    def batchSend(self,requests):

        for key, request in requests.items():
            self.send(request)

