
import importlib

class CommonUtil:

    # 获取类或对象的自定义属性
    # <B> 说明： </B>
    # <pre>
    # 略
    # </pre>
    @classmethod
    def setAttrs(cls, object, attrDict={}):
        for attr in attrDict:
            setattr(object, attr, attrDict[attr])

    @classmethod
    def ucfirst(self, str):

        return str[0].upper() + str[1:]

    # 获取上一级目录
    # <B> 说明： </B>
    # <pre>
    # 略
    # </pre>
    @classmethod
    def dirname(cls, clazz):

        packageClass = clazz.rsplit('.', 1)

        return packageClass[0];

    # 获取类或对象的自定义属性
    # <B> 说明： </B>
    # <pre>
    # 略
    # </pre>
    @classmethod
    def getClassMeta(cls, clazz):
        if type(clazz) == str:
            packageClass = clazz.rsplit('.', 1)
            try:
                packageMeta = importlib.import_module(packageClass[0])
            except Exception as e:
                raise e
                pass
            return getattr(packageMeta, packageClass[1])
        else:
            return clazz

